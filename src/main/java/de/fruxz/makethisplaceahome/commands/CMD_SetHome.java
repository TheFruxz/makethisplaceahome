package de.fruxz.makethisplaceahome.commands;

import de.fruxz.makethisplaceahome.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_SetHome implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = ((Player) sender).getPlayer();
            if(player.hasPermission("MTPAH.sethome")) {
                if (args.length == 0) {
                    Main.homes.setHome(player.getUniqueId(), player.getLocation());
                    player.sendMessage("§8>> §aSuccessfully set your home to your current location!");
                } else if (args.length == 1) {
                    OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
                    Main.homes.setHome(offlinePlayer.getUniqueId(), player.getLocation());
                    player.sendMessage("§8>> §aSuccessfully set the home of player §6" + args[0] + "§a to your current location!");
                } else
                    sender.sendMessage("§8>> §cIncorrect Syntax!");
            } else
                sender.sendMessage("§8>> §cYou don't have enough permissions!");
        } else
            sender.sendMessage("§8>> §cCommand not for console!");
        return true;
    }
}
