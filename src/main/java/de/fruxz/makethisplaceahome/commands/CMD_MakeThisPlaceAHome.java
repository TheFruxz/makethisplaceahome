package de.fruxz.makethisplaceahome.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CMD_MakeThisPlaceAHome implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage("§8>> §3§lMakeThisPlaceAHome §7developed by §6Fruxz (thefruxz)§7!");
        return true;
    }
}
