package de.fruxz.makethisplaceahome.commands;

import de.fruxz.makethisplaceahome.main.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_Home implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = ((Player) sender).getPlayer();
            if(player.hasPermission("MTPAH.tphome")) {
                if(Main.homes.getHome(player.getUniqueId()) != null) {
                     player.teleport(Main.homes.getHome(player.getUniqueId()));
                     player.sendMessage("§8>> §aYou are now at your home-point!");
                } else
                    sender.sendMessage("§8>> §cYou don't have an home-point!");
            } else
                sender.sendMessage("§8>> §cYou don't have enough permission to do that!");
        } else
            sender.sendMessage("§8>> §cThis command is only for players!");
        return true;
    }
}
