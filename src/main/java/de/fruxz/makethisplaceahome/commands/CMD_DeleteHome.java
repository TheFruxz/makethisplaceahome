package de.fruxz.makethisplaceahome.commands;

import de.fruxz.makethisplaceahome.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_DeleteHome implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender.hasPermission("MTPAH.deletehome")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    if (Main.homes.deleteHome(player.getUniqueId())) {
                        player.sendMessage("§8>> §aYou successfully deleted your home-point!");
                    } else
                        player.sendMessage("§8>> §cYou don't have an home-point!");
                } else
                    sender.sendMessage("§8>> §cNo home-point of player §6CONSOLE§c found!");
            } else if (args.length == 1) {
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
                if (Main.homes.deleteHome(offlinePlayer.getUniqueId())) {
                    sender.sendMessage("§8>> §aSuccessfully deleted home-point of this player!");
                    if (offlinePlayer.isOnline()) {
                        Player target = (Player) offlinePlayer;
                        target.sendMessage("§8>> §cYour home-point is now deleted!");
                    }
                } else
                    sender.sendMessage("§8>> §cNo home-point of player §6" + args[0] + "§c found!");
            } else
                sender.sendMessage("§8>> §cIncorrect Syntax!");
        } else
            sender.sendMessage("§8>> §cYou don't have enough permissions!");
        return true;
    }
}
