package de.fruxz.makethisplaceahome.main;

import de.fruxz.makethisplaceahome.commands.CMD_DeleteHome;
import de.fruxz.makethisplaceahome.commands.CMD_Home;
import de.fruxz.makethisplaceahome.commands.CMD_MakeThisPlaceAHome;
import de.fruxz.makethisplaceahome.commands.CMD_SetHome;
import de.fruxz.makethisplaceahome.domain.FileManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Main extends JavaPlugin {

    public static FileManager homes = new FileManager("homes.yml");

    @Override
    public void onEnable() {
        getCommand("home").setExecutor(new CMD_Home());
        getCommand("sethome").setExecutor(new CMD_SetHome());
        getCommand("sethome").setTabCompleter(new TabCompleter() {
            @Override
            public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
                if(args.length == 1) {
                    ArrayList<String> v = new ArrayList<>();
                    for(OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
                        v.add(offlinePlayer.getName());
                    }
                    return v;
                }
                return Collections.singletonList(" ");
            }
        });
        getCommand("deletehome").setExecutor(new CMD_DeleteHome());
        getCommand("deletehome").setTabCompleter(new TabCompleter() {
            @Override
            public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
                if(args.length == 1) {
                    ArrayList<String> v = new ArrayList<>();
                    for(OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
                        v.add(offlinePlayer.getName());
                    }
                    return v;
                }
                return Collections.singletonList(" ");
            }
        });
        getCommand("makethisplaceahome").setExecutor(new CMD_MakeThisPlaceAHome());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
