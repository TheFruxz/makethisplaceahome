package de.fruxz.makethisplaceahome.domain;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

public class FileManager {

    private File file;
    private YamlConfiguration loader;

    public FileManager(String file) {
        this.file = new File("plugins/MakeThisPlaceAHome", file);
        this.loader = YamlConfiguration.loadConfiguration(this.file);
    }

    public void save() {
        try {
            loader.save(file);
        } catch (FileNotFoundException e) {
            loader.set("installed", true);
            try {
                loader.save(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            loader.load(file);
        } catch (FileNotFoundException e) {
            System.err.println("Maybe a bug in the config");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void set(String path, Object v) {
        load();
        loader.set(path, v);
        save();
    }

    public Object get(String path) {
        load();
        return loader.get(path);
    }

    public String getString(String path) {
        load();
        return loader.getString(path);
    }

    public Boolean getBoolean(String path) {
        load();
        return loader.getBoolean(path);
    }

    public Integer getInteger(String path) {
        load();
        return loader.getInt(path);
    }

    public Location getLocation(String path) {
        load();
        return (Location) get(path);
    }

    public Location getHome(UUID uuid) {
        load();
        return getLocation(uuid + ".home");
    }

    public void setHome(UUID uuid, Location loc) {
        load();
        set(uuid + ".home", loc);
        save();
    }

    public boolean deleteHome(UUID uuid) {
        if(getHome(uuid) != null) {
            setHome(uuid, null);
            return true;
        } else
            return false;
    }
}
